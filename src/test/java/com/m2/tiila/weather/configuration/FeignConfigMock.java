package com.m2.tiila.weather.configuration;

import com.m2.tiila.weather.repository.client.OpenWeatherClient;
import feign.Feign;
import feign.Logger;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.mock;

@Configuration
@Profile("TEST")
public class FeignConfigMock {
    @Bean
    OpenWeatherClient getOpenWeatherCLient() {
        return mock(OpenWeatherClient.class);
    }
}
