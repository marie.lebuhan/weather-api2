/*
 * OpenWeatherMap API
 * Sample OpenWeather API.
 *
 * OpenAPI spec version: 2.5
 * Contact: kpshinde25@gmail.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package dto.openweather;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.Valid;

/**
 * Weather
 */
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaJerseyServerCodegen", date = "2023-11-24T13:51:58.004723400+01:00[Europe/Paris]")public class Weather  implements Serializable  {
  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("main")
  private String main = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("icon")
  private String icon = null;

  public Weather id(Integer id) {
    this.id = id;
    return this;
  }

  /**
   * Weather condition id
   * @return id
   **/
  @JsonProperty("id")
  @Schema(example = "803", description = "Weather condition id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Weather main(String main) {
    this.main = main;
    return this;
  }

  /**
   * Group of weather parameters (Rain, Snow, Extreme etc.)
   * @return main
   **/
  @JsonProperty("main")
  @Schema(example = "Clouds", description = "Group of weather parameters (Rain, Snow, Extreme etc.)")
  public String getMain() {
    return main;
  }

  public void setMain(String main) {
    this.main = main;
  }

  public Weather description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Weather condition within the group
   * @return description
   **/
  @JsonProperty("description")
  @Schema(example = "broken clouds", description = "Weather condition within the group")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Weather icon(String icon) {
    this.icon = icon;
    return this;
  }

  /**
   * Weather icon id
   * @return icon
   **/
  @JsonProperty("icon")
  @Schema(example = "50d", description = "Weather icon id")
  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Weather weather = (Weather) o;
    return Objects.equals(this.id, weather.id) &&
        Objects.equals(this.main, weather.main) &&
        Objects.equals(this.description, weather.description) &&
        Objects.equals(this.icon, weather.icon);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, main, description, icon);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Weather {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    main: ").append(toIndentedString(main)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    icon: ").append(toIndentedString(icon)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
