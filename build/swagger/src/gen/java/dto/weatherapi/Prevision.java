/*
 * weather-api
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package dto.weatherapi;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import dto.weatherapi.WeatherState;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDate;
import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.Valid;

/**
 * Prevision
 */
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaJerseyServerCodegen", date = "2023-11-24T13:51:59.427148100+01:00[Europe/Paris]")public class Prevision  implements Serializable  {
  @JsonProperty("date")
  private LocalDate date = null;

  @JsonProperty("temperature")
  private Integer temperature = null;

  @JsonProperty("city")
  private String city = null;

  @JsonProperty("state")
  private WeatherState state = null;

  public Prevision date(LocalDate date) {
    this.date = date;
    return this;
  }

  /**
   * Get date
   * @return date
   **/
  @JsonProperty("date")
  @Schema(description = "")
  @Valid
  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public Prevision temperature(Integer temperature) {
    this.temperature = temperature;
    return this;
  }

  /**
   * Get temperature
   * @return temperature
   **/
  @JsonProperty("temperature")
  @Schema(description = "")
  public Integer getTemperature() {
    return temperature;
  }

  public void setTemperature(Integer temperature) {
    this.temperature = temperature;
  }

  public Prevision city(String city) {
    this.city = city;
    return this;
  }

  /**
   * Get city
   * @return city
   **/
  @JsonProperty("city")
  @Schema(description = "")
  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public Prevision state(WeatherState state) {
    this.state = state;
    return this;
  }

  /**
   * Get state
   * @return state
   **/
  @JsonProperty("state")
  @Schema(description = "")
  @Valid
  public WeatherState getState() {
    return state;
  }

  public void setState(WeatherState state) {
    this.state = state;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Prevision prevision = (Prevision) o;
    return Objects.equals(this.date, prevision.date) &&
        Objects.equals(this.temperature, prevision.temperature) &&
        Objects.equals(this.city, prevision.city) &&
        Objects.equals(this.state, prevision.state);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, temperature, city, state);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Prevision {\n");
    
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    temperature: ").append(toIndentedString(temperature)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
